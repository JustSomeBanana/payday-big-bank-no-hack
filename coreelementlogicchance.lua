-- Basically copied from the RNG modifier mod(https://github.com/DrNewbie/RNGModifier). Since there isn't a license included I'm gonna risk getting sued 

core:module("CoreElementLogicChance")
core:import("CoreMissionScriptElement")
ElementLogicChance = ElementLogicChance or class(CoreMissionScriptElement.MissionScriptElement)

BigBankNoHack_ElementLogicChance_on_executed = BigBankNoHack_ElementLogicChance_on_executed or ElementLogicChance.on_executed


function ElementLogicChance:on_executed(...)
	if tostring(Global.game_settings.level_id) == "big" then
		if self._id == 104494 then 
			self._chance = 999
		end
	end	
	return BigBankNoHack_ElementLogicChance_on_executed(self, ...) 
end

local BigBankNoHack_ElementLogicChanceOperator_on_executed = ElementLogicChanceOperator.on_executed

function ElementLogicChanceOperator:on_executed(...)
	if not self._values.enabled then
		return
	end
	BigBankNoHack_ElementLogicChanceOperator_on_executed(self, ...)
end
